import React, {Component} from 'react';
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

// const image = {uri: 'https://reactjs.org/logo-og.png'};

const {width, height} = Dimensions.get('screen');

export default class Login extends Component {
  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
        }}>
        <ImageBackground
          resizeMode="stretch"
          source={require('../../imgs/bg.jpg')}
          style={styles.image}>
          <TouchableOpacity style={styles.topButtonStyle}>
            <Text style={{fontSize: 35}}>Login</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              height: 50,
              backgroundColor: 'pink',
              width: '60%',
              borderTopRightRadius: 25,
              borderBottomRightRadius: 25,
              zIndex: 5,
            }}></TouchableOpacity>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  image: {
    flex: 1,
    justifyContent: 'space-around',
  },
  text: {
    color: 'white',
    fontSize: 42,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: '#000000a0',
  },
  topButtonStyle: {
    height: 50,
    backgroundColor: 'pink',
    width: '60%',
    borderTopLeftRadius: 25,
    borderBottomLeftRadius: 25,
    zIndex: 5,
    position: 'absolute',
    right: 0,
    zIndex: 100,
  },
});
