import React, {Component} from 'react';
// import React, {useState} from 'react';

import {
  Text,
  View,
  SafeAreaViewBase,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';

export default class MyownPage extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View
          style={{flex: 1, backgroundColor: 'violet', flexDirection: 'row'}}>
          <Text
            style={{
              fontSize: 30,
              paddingTop: 20,
              paddingLeft: 10,
            }}>
            Homey
          </Text>
          <View style={{flex: 1, backgroundColor: 'violet'}}></View>
          <Text style={{fontSize: 25, paddingTop: 20, paddingRight: 20}}>
            {' '}
            setting
          </Text>
        </View>
        <View
          style={{flex: 1, backgroundColor: 'blue', justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: 30,
              //   justifyContent: 'center',
              //   backgroundColor: 'white',
              textAlign: 'center',
              fontWeight: 'bold',
              fontStyle: 'italic',
            }}>
            WELCOME HOME!
          </Text>
        </View>
        <View style={{flex: 3}}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <TouchableOpacity
              style={{
                flex: 1,
                backgroundColor: 'violet',
                // borderStartColor: 'blue',

                borderRadius: 15,
                marginRight: 12,
                marginLeft: 10,
              }}>
              <Text style={styles.icons}>Bedroom</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                flex: 1,
                backgroundColor: 'violet',
                borderRadius: 15,
                marginRight: 12,
              }}>
              <Text style={styles.icons}>Living room</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flex: 1,
                backgroundColor: 'violet',
                borderRadius: 15,
                marginRight: 12,
              }}>
              <Text style={styles.icons}>Kitchen</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <TouchableOpacity
              style={{
                flex: 1,
                backgroundColor: 'violet',
                borderRadius: 15,
                marginRight: 12,
                marginLeft: 12,
                marginTop: 12,
              }}>
              <Text style={styles.icons}>Bathroom</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flex: 1,
                backgroundColor: 'violet',
                borderRadius: 15,
                marginRight: 12,
                marginTop: 12,
              }}>
              <Text style={styles.icons}>Kids room</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flex: 1,
                backgroundColor: 'violet',
                borderRadius: 15,
                marginRight: 12,
                marginTop: 12,
              }}>
              <Text style={styles.icons}>Dinning room</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flex: 2, paddingTop: 30}}>
          <View style={{flex: 1}}>
            <Text style={{fontSize: 30, paddingTop: 20}}>
              {' '}
              💡 Light Control
            </Text>
          </View>
          <View style={{flex: 1}}>
            <Text style={{fontSize: 30, paddingTop: 20}}> 🔌 Power Supply</Text>
          </View>
        </View>
        <View
          style={{
            flex: 2,
            // backgroundColor: 'violet',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 35, paddingLeft: 80, paddingBottom: 70}}>
            Security Level
          </Text>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'blue',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  text: {
    fontSize: 30,
    backgroundColor: 'gold',
    paddingVertical: 10,
    paddingHorizontal: 20,
    // paddingVertical: 10,
    color: 'red',
    borderRadius: 20,
    textTransform: 'uppercase',
  },
  click: {
    fontSize: 30,
    backgroundColor: 'silver',
    paddingVertical: 5,
    paddingHorizontal: 20,
    // paddingVertical: 10,
    color: 'red',
    borderRadius: 30,
    textTransform: 'uppercase',
    marginTop: 30,
  },
  icons: {
    fontSize: 20,
    paddingLeft: 21,
    paddingTop: 45,
  },
});
