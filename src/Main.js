import React, {Component} from 'react';
import {Text, View, SafeAreaView, StatusBar} from 'react-native';
import RouterComponent from './Router';

export default class Main extends Component {
  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <StatusBar barStyle="dark-content" backgroundColor="black"></StatusBar>
        <View style={{flex: 1}}>
          <RouterComponent />
        </View>
      </SafeAreaView>
    );
  }
}
