import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Scene, Router, Stack} from 'react-native-router-flux';
import Login from './Component/Login/Login';
import ForgotPass from './Component/Login/ForgotPass';
import ChangePass from './Component/Login/ChangePass';
import MyownPage from './Component/Login/MyownPage';

// const App = () => {
//     return {
//       // <View style={{flex: 1}}>
//       //   <Router>
//       //     <Scene
//       //       key="main"
//       //       navigationBarStyle={{backgroundColor: 'green'}}
//       //       titleStyle={{
//       //         color: 'white',
//       //         fontSize: 20,
//       //         fontWeight: 'bold',
//       //         backButtonTintColor: 'white',
//       //       }}>
//       //       <Scene
//       //         key="Login"
//       //         component={Login}
//       //         backTitle=""
//       //         hideNavBar></Scene>
//       //       <Scene
//       //         key="Forgot"
//       //         component={ForgotPass}
//       //         backTitle=""
//       //         hideNavBar></Scene>
//       //       <Scene
//       //         key="Change"
//       //         component={ChangePass}
//       //         backTitle=""
//       //         hideNavBar></Scene>
//       //     </Scene>
//       //   </Router>
//       // </View>
//       <Router>
//         <Stack key="root">
//           <Scene key="login" component={Login} title="Login" />
//           <Scene key="register" component={ForgotPass} title="Register" />
//           <Scene key="home" component={ChangePass} />
//         </Stack>
//       </Router>
//     }
//   }

const RouterComponent = () => (
  <Router>
    <Stack key="root">
      <Scene key="login" component={Login} title="Login" />
      <Scene key="register" component={ForgotPass} title="Register" />
      <Scene key="home" component={ChangePass} />
      <Scene key="MyownPage" component={MyownPage} />
    </Stack>
  </Router>
);

export default RouterComponent;
